# RT

Created for [RoguelikeDev Does The Complete Roguelike Tutorial](https://www.reddit.com/r/roguelikedev/wiki/python_tutorial_series/#wiki_roguelikedev_does_the_complete_roguelike_tutorial), following the official [Python tutorials](http://rogueliketutorials.com/tutorials/tcod/).

![Logo](https://i.imgur.com/xSph4zw.png)

Checkout the [2021 Branch](https://gitlab.com/giakki/rt/-/tree/2021), which is ported from Python to C
